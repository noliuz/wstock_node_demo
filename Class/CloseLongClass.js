const Sequelize = require('sequelize')
const {seqConn} = require('./seqConn')
const {CloseLongModel} = require('./models');

class CloseLongClass {
  constructor() {

  }

  create(res,long_buy_id,price) {
    CloseLongModel.create({
      long_buy_id:long_buy_id,
      price:price,

    }).then(()=>{
      res.send('ok')
    })
  }

  update(res,id,long_buy_id,price) {
    let data = {
      long_buy_id:long_buy_id,
      price:price,
    }
    CloseLongModel.update(data, {
          where: {
            id:id
          }
    }).then(() => {
      res.send('ok')
    })
  }

  delete(res,id) {
    CloseLongModel.destroy({
      where: {
        id: id
      }
    }).then(() => {
      res.send('ok')
    })

  }

  async get(res,id) {
    let mres

    if (id != -1) {
       mres = await CloseLongModel.findAll({
        where: {
          id:id
        }
      })
    } else {
      mres = await CloseLongModel.findAll();
    }

    res.json(mres)
  }

}

module.exports = CloseLongClass
