const Sequelize = require('sequelize')
const {seqConn} = require('./seqConn')
const {BuyModel,SellModel,ShortSellModel,CloseShortModel} = require('./models');

class CalculateClass {
  constructor() {

  }

  async buyLeftInPortfolio() {
    //get buy data
    let buyRes = await BuyModel.findAll({
    })

    //get sell data
    var leftArr = []
    for (let i=0;i<buyRes.length;i++) {
      let buy_id = buyRes[i].id
      let sellRes = await SellModel.findAll({
        where: {
          buy_id:buy_id
        }
      })
      //add no sell
      if (sellRes.length == 0) {
        leftArr.push(buyRes[i])
      }
    }

    return leftArr
  }

  async shortLeftInPortfolio() {
    //get buy data
    let shortRes = await ShortSellModel.findAll({
    })

    //get sell data
    var leftArr = []
    for (let i=0;i<shortRes.length;i++) {
      let short_id = shortRes[i].id
      let closeShortRes = await CloseShortModel.findAll({
        where: {
          short_sell_id:short_id
        }
      })
      //add no sell
      if (closeShortRes.length == 0) {
        leftArr.push(shortRes[i])
      }
    }

    return leftArr
  }

  async profitList() {
    //get buy data
    let buyRes = await BuyModel.findAll({
    })

    //get sell data
    var profitRes = []
    for (let i=0;i<buyRes.length;i++) {
      let buy_id = buyRes[i].id
      let sellRes = await SellModel.findAll({
        where: {
          buy_id:buy_id
        }
      })

      //check if there is selling
      if (sellRes.length != 0) {
        let profit = {
          symbol : buyRes[i].symbol,
          profit : sellRes[0].price - buyRes[i].price,
          buy_date : buyRes[i].updatedAt,
          sell_date : sellRes[0].updatedAt
        }
        profitRes.push(profit)
      }
    }

    return profitRes
  }
}

module.exports = CalculateClass
