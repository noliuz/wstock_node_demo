const Sequelize = require('sequelize')
const {seqConn} = require('./seqConn')
const {ShortSellModel} = require('./models');

class ShortSellClass {
  constructor() {

  }

  create(res,symbol,amount,price,stop_loss,target_price) {
    ShortSellModel.create({
      symbol:symbol,
      amount:amount,
      price:price,
      stop_loss:stop_loss,
      target_price:target_price
    }).then(()=>{
      res.send('ok')
    })
  }

  update(res,id,symbol,amount,price,stop_loss,target_price) {
    let data = {
      symbol:symbol,
      amount:amount,
      price:price,
      stop_loss:stop_loss,
      target_price:target_price
    }
    ShortSellModel.update(data, {
          where: {
            id:id
          }
    }).then(() => {
      res.send('ok')
    })
  }

  delete(res,id) {
    ShortSellModel.destroy({
      where: {
        id: id
      }
    }).then(() => {
      res.send('ok')
    })

  }

  async get(res,id) {
    let mres

    if (id != -1) {
       mres = await ShortSellModel.findAll({
        where: {
          id:id
        }
      })
    } else {
      mres = await ShortSellModel.findAll();
    }

    res.json(mres)
  }

}

module.exports = ShortSellClass
