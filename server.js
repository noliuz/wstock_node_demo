var express = require('express');
var app = express();
const {BuyModel,SellModel,LongBuyModel} = require('./Class/models.js');

const BuyClass = require('./Class/BuyClass.js')
const SellClass = require('./Class/SellClass.js')
const LongBuyClass = require('./Class/LongBuyClass.js')
const ShortSellClass = require('./Class/ShortSellClass.js')
const CloseLongClass = require('./Class/CloseLongClass.js')
const CloseShortClass = require('./Class/CloseShortClass.js')
const CalculateClass = require('./Class/CalculateClass.js')

app.get('/buy_left_in_port_list', function (req, res) {
  let cc = new CalculateClass()
  cc.buyLeftInPortfolio()
    .then((arr) => {
      res.send(res.json(arr))

     })
})

app.get('/short_left_in_port_list', function (req, res) {
  let cc = new CalculateClass()
  cc.shortLeftInPortfolio()
    .then((arr) => {
      res.send(res.json(arr))
     })
})


app.get('/profit_list', function (req, res) {
  let cc = new CalculateClass()
  cc.profitList()
    .then((arr) => {
      res.send(res.json(arr))

     })
})

//BUY
var buy = new BuyClass()
app.get('/buy/create/:symbol/:amount/:price/:stop_loss/:target_price', function (req, res) {
   let symbol = req.params.symbol
   let amount = req.params.amount
   let price = req.params.price
   let stop_loss = req.params.stop_loss
   let target_price = req.params.target_price

   buy.create(res,symbol,amount,price,stop_loss,target_price)

})

app.get('/buy/update/:id/:symbol/:amount/:price/:stop_loss/:target_price', function (req, res) {
  let id = req.params.id
  let symbol = req.params.symbol
  let amount = req.params.amount
  let price = req.params.price
  let stop_loss = req.params.stop_loss
  let target_price = req.params.target_price

  buy.update(res,id,symbol,amount,price,stop_loss,target_price)

})
app.get('/buy/delete/:id', function (req, res) {
  buy.delete(res,req.params.id)
})
app.get('/buy/get/:id', async function(req, res) {
  buy.get(res,req.params.id)
})
///////////////////////END BUY

//SELL
var sell = new SellClass()
app.get('/sell/create/:buy_id/:price', function (req, res) {
   let buy_id = req.params.buy_id
   let price = req.params.price

   sell.create(res,buy_id,price)

})

app.get('/sell/update/:id/:buy_id/:price', function (req, res) {
  let id = req.params.id
  let buy_id = req.params.buy_id
  let price = req.params.price

  sell.update(res,id,buy_id,price)

})
app.get('/sell/delete/:id', function (req, res) {
  sell.delete(res,req.params.id)
})
app.get('/sell/get/:id', async function(req, res) {
  sell.get(res,req.params.id)
})
//////////////////END SELL

/////////////Open Short
var short = new ShortSellClass()
app.get('/short/create/:symbol/:amount/:price/:stop_loss/:target_price', function (req, res) {
   let symbol = req.params.symbol
   let amount = req.params.amount
   let price = req.params.price
   let stop_loss = req.params.stop_loss
   let target_price = req.params.target_price

   short.create(res,symbol,amount,price,stop_loss,target_price)

})

app.get('/short/update/:id/:symbol/:amount/:price/:stop_loss/:target_price', function (req, res) {
  let id = req.params.id
  let symbol = req.params.symbol
  let amount = req.params.amount
  let price = req.params.price
  let stop_loss = req.params.stop_loss
  let target_price = req.params.target_price

  short.update(res,id,symbol,amount,price,stop_loss,target_price)

})
app.get('/short/delete/:id', function (req, res) {
  short.delete(res,req.params.id)
})
app.get('/short/get/:id', async function(req, res) {
  short.get(res,req.params.id)
})
/////////////Open Short end

/////////////Close Short
var cshort = new CloseShortClass()
app.get('/close_short/create/:short_sell_id/:price', function (req, res) {
   let short_sell_id = req.params.short_sell_id
   let price = req.params.price

   cshort.create(res,short_sell_id,price)

})

app.get('/close_short/update/:id/:short_sell_id/:price', function (req, res) {
  let id = req.params.id
  let short_sell_id = req.params.short_sell_id
  let price = req.params.price

  cshort.update(res,id,short_sell_id,price)

})
app.get('/close_short/delete/:id', function (req, res) {
  cshort.delete(res,req.params.id)
})
app.get('/close_short/get/:id', async function(req, res) {
  cshort.get(res,req.params.id)
})
//////////////Close short end

/////////////Open Long
var long = new LongBuyClass()
app.get('/long/create/:symbol/:amount/:price/:stop_loss/:target_price', function (req, res) {
   let symbol = req.params.symbol
   let amount = req.params.amount
   let price = req.params.price
   let stop_loss = req.params.stop_loss
   let target_price = req.params.target_price

   long.create(res,symbol,amount,price,stop_loss,target_price)

})

app.get('/long/update/:id/:symbol/:amount/:price/:stop_loss/:target_price', function (req, res) {
  let id = req.params.id
  let symbol = req.params.symbol
  let amount = req.params.amount
  let price = req.params.price
  let stop_loss = req.params.stop_loss
  let target_price = req.params.target_price

  long.update(res,id,symbol,amount,price,stop_loss,target_price)

})
app.get('/long/delete/:id', function (req, res) {
  long.delete(res,req.params.id)
})
app.get('/long/get/:id', async function(req, res) {
  long.get(res,req.params.id)
})
/////////////Open Long end

/////////////Close Long
var clong = new CloseLongClass()
app.get('/close_long/create/:long_buy_id/:price', function (req, res) {
   let long_buy_id = req.params.long_buy_id
   let price = req.params.price

   clong.create(res,long_buy_id,price)
})

app.get('/close_long/update/:id/:long_buy_id/:price', function (req, res) {
  let id = req.params.id
  let long_buy_id = req.params.long_buy_id
  let price = req.params.price

  clong.update(res,id,long_buy_id,price)

})
app.get('/close_long/delete/:id', function (req, res) {
  clong.delete(res,req.params.id)
})
app.get('/close_long/get/:id', async function(req, res) {
  clong.get(res,req.params.id)
})

//////////////Close short end


//Comnination Get
app.get('/get_sell_list', async function(req, res) {

  res.send('ok')
})

var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port

   console.log("Example app listening at http://%s:%s", host, port)
})
